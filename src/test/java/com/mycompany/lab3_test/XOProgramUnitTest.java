/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3_test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Paramet_PC
 */
public class XOProgramUnitTest {

    public XOProgramUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayByO() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2ByO() {
        String[][] table = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1ByX() {
        String[][] table = {{"X", "X", "X"}, {"-", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3ByX() {
        String[][] table = {{"-", "O", "O"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow2ByX() {
        String[][] table = {{"-", "O", "O"}, {"X", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow1ByO() {
        String[][] table = {{"O", "O", "O"}, {"-", "X", "X"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinRow3ByO() {
        String[][] table = {{"-", "X", "X"}, {"-", "-", "-"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1ByO() {
        String[][] table = {{"O", "X", "X"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2ByO() {
        String[][] table = {{"X", "O", "X"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3ByO() {
        String[][] table = {{"X", "X", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol1ByX() {
        String[][] table = {{"X", "O", "O"}, {"X", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol2ByX() {
        String[][] table = {{"O", "X", "O"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinCol3ByX() {
        String[][] table = {{"O", "O", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinNoPlayByX() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDraw() {
        String[][] table = {{"X", "O", "X"}, {"O", "O", "X"}, {"X", "X", "O"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkDraw(table));
    }

    @Test
    public void testCheckDiagonal1ByX() {
        String[][] table = {{"O", "O", "X"}, {"O", "X", "O"}, {"X", "O", "O"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckDiagonal1ByO() {
        String[][] table = {{"X", "X", "O"}, {"X", "O", "X"}, {"O", "X", "X"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckDiagonal2ByX() {
        String[][] table = {{"X", "O", "O"}, {"O", "X", "O"}, {"O", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckDiagonal2ByO() {
        String[][] table = {{"O", "X", "X"}, {"X", "O", "X"}, {"X", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWin(table, currentPlayer));
    }



}
